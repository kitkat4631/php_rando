<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
            $servname = 'localhost';
            $dbname = 'reunion_island';
            $user = 'root';
            $pass = 'root';
            
            try{
                $dbco = new PDO("mysql:host=$servname;dbname=$dbname", $user, $pass);
                $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                
                $dbco->beginTransaction();
                
                $sql1 = "INSERT INTO hiking('name', 'difficulty', 'distance', 'duration', 'height_difference')
                        VALUES('Le tour de l'île de Moyenne aux Seychelles','Très facile','1.5km','1h',NULL)";
                $dbco->exec($sql1);
                
                $sql2 = "INSERT INTO hiking('name', 'difficulty', 'distance', 'duration', 'height_difference')
                        VALUES('Le sommet du Piton Béthoune par le tour du Bonnet de Prêtre','Très difficile','5.7km','4h','650M')";
                $dbco->exec($sql2);

                $sql3 = "INSERT INTO hiking('name', 'difficulty', 'distance', 'duration', 'height_difference')
                        VALUES('La cascade de Trois Roches par le Taïbit et Marla','Difficile','19km','8H30','1800M')";
                $dbco->exec($sql3);

                $sql4 = "INSERT INTO hiking('name', 'difficulty', 'distance', 'duration', 'height_difference')
                        VALUES('Le Sentier des Sources entre Cilaos et Bras Sec','Facile','4.8km','1H15','280M')";
                $dbco->exec($sql4);

                $sql5 = "INSERT INTO hiking('name', 'difficulty', 'distance', 'duration', 'height_difference')
                        VALUES('L/'Ilet des Salazes et le point de vue de Cap Bouteille','Moyen','8KM','3H30','750M')";
                $dbco->exec($sql5);
                $dbco->commit();
                echo 'Entrées ajoutées dans la table';
            }
            
            catch(PDOException $e){
                $dbco->rollBack();
              echo "Erreur : " . $e->getMessage();
            }
        ?>
</body>
</html>