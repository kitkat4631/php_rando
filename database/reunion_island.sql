id,name,difficulty,distance,duration,height_difference
1,"Le tour de l'île de Moyenne aux Seychelles","Très facile",1.5km,1h,NULL
2,"Le sommet du Piton Béthoune par le tour du Bonnet de Prêtre","Très difficile",5.7km,4h,650M
3,"La cascade de Trois Roches par le Taïbit et Marla",Difficile,19km,8H30,1800M
4,"Le Sentier des Sources entre Cilaos et Bras Sec",Facile,4.8km,1H15,280M
5,"L/'Ilet des Salazes et le point de vue de Cap Bouteille",Moyen,8KM,3H30,750M
