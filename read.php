<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Randonnées</title>
    <link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
  </head>
  <body>
    <h1>Liste des randonnées</h1>
    <?php
            $servname = 'localhost';
            $dbname = 'reunion_island';
            $user = 'root';
            $pass = 'root';
            
            try{
                $dbco = new PDO("mysql:host=$servname;dbname=$dbname", $user, $pass);
                $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $e){
              $dbco->rollBack();
            echo "Erreur : " . $e->getMessage();
          }
    ?>
    <table>
         <!-- Afficher la liste des randonnées -->
         <tr>
           <td>
          <?php  $query = $dbco->query('SELECT name FROM hiking');
                 $name = $query->fetchAll();
                 print_r($name); ?>
           </td>
         </tr>
    </table>
  </body>
</html>
